package nl.mrchoi;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Given an RGB(A) CSS color, determine whether its format is valid 
or not. Create a function that takes a string (e.g. “rgb(0, 0, 0)”, or 
“rgba(50,255,0,0.5)”) and return true if it's format is correct, 
otherwise return false. Look at the tests for specifics on 
functionality
*/

public class ColourUtil {
    public static boolean validRgbColour(String rgb) {
        Pattern rgbDigits = Pattern.compile("rgb\\(\\s*(\\d+)\\s*,\\s*(\\d+)\\s*,\\s*(\\d+)\\s*\\)");
        Pattern rgbPercentages = Pattern.compile("rgb\\(\\s*(\\d+)%\\s*,\\s*(\\d+)%\\s*,\\s*(\\d+)%\\s*\\)");
        Pattern rgba = Pattern.compile("rgba\\(\\s*(\\d+)\\s*,\\s*(\\d+)\\s*,\\s*(\\d+)\\s*,\\s*(\\d+(?:\\.\\d+)?|\\.\\d+)\\s*\\)");

        Matcher m1 = rgbDigits.matcher(rgb);
        Matcher m2 = rgbPercentages.matcher(rgb);
        Matcher m3 = rgba.matcher(rgb);

        if (m1.matches()) {
            return Integer.parseInt(m1.group(1)) >= 0 && Integer.parseInt(m1.group(2)) >= 0 && Integer.parseInt(m1.group(3)) >= 0 &&
            Integer.parseInt(m1.group(1)) < 256 && Integer.parseInt(m1.group(2)) < 256 && Integer.parseInt(m1.group(3)) < 256;
        }
        if (m2.matches()) {
            return Integer.parseInt(m2.group(1)) >= 0 && Integer.parseInt(m2.group(2)) >= 0 && Integer.parseInt(m2.group(3)) >= 0 &&
            Integer.parseInt(m2.group(1)) <= 100 && Integer.parseInt(m2.group(2)) <= 100 && Integer.parseInt(m2.group(3)) <= 100;
        }
        if (m3.matches()) {
            return Integer.parseInt(m3.group(1)) >= 0 && Integer.parseInt(m3.group(2)) >= 0 && Integer.parseInt(m3.group(3)) >= 0 &&
            Integer.parseInt(m3.group(1)) < 256 && Integer.parseInt(m3.group(2)) < 256 && Integer.parseInt(m3.group(3)) < 256 &&
            Double.parseDouble(m3.group(4)) >= 0.0 && Double.parseDouble(m3.group(4)) <= 1.0;
        }
        return false;
    }
}
