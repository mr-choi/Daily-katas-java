package nl.mrchoi;

/*
KATA: Largest difference in Array
The goal of this kata is to make use of Array manipulation 
techniques in Java. You are given an array containing integers. 
You are to find the maximum difference between two elements. 
The smaller element must appear before larger element.
Example:
Input:
numbers = { 2, 3, 1, 7, 9, 5, 11, 3, 5 }
Output:
The maximum difference between two array elements is:
10
Explanation:
10 = 11(6) – 1(2)
*/

public class LargestDifference 
{
    public static int largestDifference( int[] numbers )
    {
        int[] smallestFromLeft = new int[numbers.length];
        int[] largestFromRight = new int[numbers.length];
        int smallest = numbers[0];
        int largest = numbers[numbers.length-1];

        for(int i=0; i<numbers.length; i++) {
            smallestFromLeft[i] = smallest = Math.min(smallest, numbers[i]);
            largestFromRight[numbers.length-i-1] = largest = Math.max(largest, numbers[numbers.length-i-1]);
        }
        int result = 0;
        for(int i=0; i<numbers.length; i++) {
            result = Math.max(result, largestFromRight[i]-smallestFromLeft[i]);
        }

        return result;
    }
}
