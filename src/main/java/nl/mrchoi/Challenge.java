package nl.mrchoi;
/*
Write a function that accepts a positive integer between 0 and 
999 inclusive and returns a string representation of that integer 
written in English.
There are no hyphens used (e.g. "thirty five" not "thirty-five"). The
word "and" is not used (e.g. "one hundred one" not "one hundred 
and one").
*/
public class Challenge {
    public static final String[] zeroToTwenty = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
    "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty"};
    public static final String[] unitsOfTen = {"zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

    public static String numToEng(int number) {
        StringBuilder sb = new StringBuilder();
        int hundreds = number / 100;
        if (hundreds > 0) {
            sb.append(zeroToTwenty[hundreds]);
            sb.append(" hundred");
            if (number % 100 == 0) return sb.toString();
            sb.append(" ");
        }
        int remain = number % 100;
        if (remain < 20) {
            sb.append(zeroToTwenty[remain]);
            return sb.toString();
        }
        int tens = remain / 10;
        sb.append(unitsOfTen[tens]);
        int units = remain % 10;
        if (units > 0) {
            sb.append(" ");
            sb.append(zeroToTwenty[units]);
        }
        return sb.toString();
    }

    public static void main(String... args) {
        for (int i = 0; i < 1000; i++) {
            System.out.println(numToEng(i));
        }
    }
}
