package nl.mrchoi;

import java.util.regex.Pattern;

/*
Create a program that converts a phone number with letters to one with only 
numbers. The letter should be converted into the corresponding number on a
classic telephone keypad 
*/

public class TelephoneNumber {
    public static String textToNum(String number) {

        String[] map = new String['Z'+1];
        map['A'] = map['B'] = map['C'] = "2";
        map['D'] = map['E'] = map['F'] = "3";
        map['G'] = map['H'] = map['I'] = "4";
        map['J'] = map['K'] = map['L'] = "5";
        map['M'] = map['N'] = map['O'] = "6";
        map['P'] = map['Q'] = map['R'] = map['S'] = "7";
        map['T'] = map['U'] = map['V'] = "8";
        map['W'] = map['X'] = map['Y'] = map['Z'] = "9";

        Pattern letter = Pattern.compile("[A-Z]");
        return letter.matcher(number).replaceAll(m -> map[m.group(0).charAt(0)]);
    }
}
