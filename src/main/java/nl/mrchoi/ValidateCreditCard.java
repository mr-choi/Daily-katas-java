package nl.mrchoi;

import java.util.Collections;
import java.util.Stack;

public class ValidateCreditCard {
    public static boolean validate(String number) {
        if (number.length() < 14 || number.length() > 19) return false;
        Stack<Integer> digits = new Stack<>();
        for (char c : number.toCharArray()) {
            digits.add(c - '0');
        }
        int controlDigit = digits.pop();
        Collections.reverse(digits);
        for (int i = 0; i < digits.size(); i+=2) {
            digits.set(i, digits.get(i)*2 / 10 + digits.get(i)*2 % 10);
        }
        int sum = digits.stream().mapToInt(i->i).sum();
        return controlDigit == 10 - sum % 10;
    }
}
