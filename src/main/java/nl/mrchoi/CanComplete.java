package nl.mrchoi;

import java.util.regex.Pattern;

public class CanComplete {
    public static boolean canComplete(String input, String word) {
        String regex = String.join(".*", input.split(""));
        return Pattern.compile(regex).matcher(word).find();
    }
}
