package nl.mrchoi;

import java.util.regex.Pattern;

/*
For this exercise, keep in mind the following definitions:
1. A term is either an initials or word.
2. initials = 1 character
3. words = 2+ characters (no dots allowed)
A valid name is a name written in one of the following ways:
H. Wells
H. G. Wells
Herbert G. Wells
Herbert George Wells
The following names are invalid:
Herbert or Wells (single names not allowed)
H Wells or H. G Wells (initials must end with dot)
h. Wells or H. wells or h. g. Wells (incorrect capitalization)
H. George Wells (middle name expanded, while first still left as initial)
H. G. W. (last name is not a word)
Herb. G. Wells (dot only allowed after initial, not word)
Rules
Both initials and words must be capitalized.
Initials must end with a dot.
A name must be either 2 or 3 terms long.
If the name is 3 words long, you can expand the first and middle name 
or expand the first name only. You cannot keep the first name as an 
initial and expand the middle name only.
The last name must be a word (not an initial).
Your task is to write a function that determines whether a name is valid or 
not. Return true if the name is valid, false otherwise.
*/

public class ValidName {
    public static boolean validName(String name) {
        String[] terms = name.split(" ");
        if (terms.length < 2 || terms.length > 3) return false;
        Pattern initial = Pattern.compile("[A-Z]\\.");
        Pattern word = Pattern.compile("[A-Z][a-z]+");

        if (!word.matcher(terms[terms.length-1]).matches()) return false;
        
        boolean initialsAllowed = true;
        for (int i = terms.length-2; i>=0; i--) {
            if (word.matcher(terms[i]).matches()) {
                initialsAllowed = false;
            }
            else if (!(initialsAllowed && initial.matcher(terms[i]).matches())) return false;
        }

        return true;
    }
}
