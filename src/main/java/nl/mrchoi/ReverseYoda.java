package nl.mrchoi;

/*
Create a function that takes a string with at least one sentence in 
it and returns a string with the part(s) after the comma at the 
beginning of the sentence(s).
*/

public class ReverseYoda {
    public static String reverseYoda(String text) {
        String[] sentences = text.split("\\.\\s");
        String[] reverseYoda = new String[sentences.length];
        for (int i = 0; i < reverseYoda.length; i++) {
            reverseYoda[i] = reverseYodaSingleSentence(sentences[i]);
        }
        return String.join(". ", reverseYoda) + ".";
    }
    public static String reverseYodaSingleSentence(String sentence) {
        sentence = sentence.replaceAll("\\.", "");
        String[] parts = sentence.split(",\\s");
        return parts[1].substring(0,1).toUpperCase() + parts[1].substring(1)
        + " " + parts[0].substring(0,1).toLowerCase() + parts[0].substring(1);
    }
}
