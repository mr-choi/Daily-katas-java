package nl.mrchoi;

import java.util.regex.Pattern;

public class PasswordStrength {
    public static String passwordStrength(String password) {
        Pattern whitespace = Pattern.compile("\\s");
        if (password.length() < 6 || whitespace.matcher(password).find()) return "Invalid";
        int criteriaMet = 0;
        Pattern digits = Pattern.compile("\\d");
        Pattern lowercase = Pattern.compile("[a-z]");
        Pattern uppercase = Pattern.compile("[A-Z]");
        Pattern special = Pattern.compile("\\W");
        if (password.length() >= 8) criteriaMet++;
        if (digits.matcher(password).find()) criteriaMet++;
        if (lowercase.matcher(password).find()) criteriaMet++;
        if (uppercase.matcher(password).find()) criteriaMet++;
        if (special.matcher(password).find()) criteriaMet++;
        
        if (criteriaMet < 3) return "Weak";
        if (criteriaMet < 5) return "Moderate";
        return "Strong";
    }
}
