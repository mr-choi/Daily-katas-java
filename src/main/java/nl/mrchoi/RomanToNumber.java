package nl.mrchoi;

/*
Write a function to convert from Roman Numerals to decimal 
numerals. You don’t need to worry about validating the input
*/
public class RomanToNumber {
    public static long romanToNumber(String roman) {
        int[] romanToNumberMap = new int['Z'];
        romanToNumberMap['I'] = 1;
        romanToNumberMap['V'] = 5;
        romanToNumberMap['X'] = 10;
        romanToNumberMap['L'] = 50;
        romanToNumberMap['C'] = 100;
        romanToNumberMap['D'] = 500;
        romanToNumberMap['M'] = 1000;

        long result = 0;
        int current = 1;

        for (int i = roman.length()-1; i >= 0; i--) {
            int value = romanToNumberMap[roman.charAt(i)];
            if (value < current) {
                result -= value;
            } else {
                result += value;
                current = value;
            }
        }

        return result;
    }
}
