package nl.mrchoi;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;


public class FreddyFridays {
    public static int freddyFridays(int year) {
        int friday13s = 0;
        for (int month=1; month<=12; month++) {
            LocalDate date = LocalDate.of(year, month, 13);
            if (date.getDayOfWeek() == DayOfWeek.FRIDAY) friday13s++;
        }
        return friday13s;
    }







    


    public static void main(String[] args) {
        int maxFriday13s = -1;
        ArrayList<Integer> years = new ArrayList<>();
        for (int year=2022; year<=3000; year++) {
            int result = freddyFridays(year);
            if (result < maxFriday13s) continue;
            if (result == maxFriday13s) years.add(year);
            if (result > maxFriday13s) {
                maxFriday13s = result;
                years.clear();
                years.add(year);
            }
        }
        System.out.print("The maximum number of friday 13ths is ");
        System.out.println(maxFriday13s);
        System.out.println("The years of occurrence: ");
        System.out.println(years);
    }
}
