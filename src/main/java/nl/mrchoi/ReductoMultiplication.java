package nl.mrchoi;

/*
Create a function that takes numbers as arguments, adds them 
together, and returns the product of digits until the answer is only
1 digit long.
Example
sumDigProd(16, 28) ➞ 6
16 + 28 = 44
4 * 4 =  16
1 * 6 = 6
Notes
The input of the function is at least one number but can be any 
amount of numbers (Varargs)
*/

public class ReductoMultiplication {
    public static int sumDigProd(int... numbers) {
        int sum = 0;
        for (int n : numbers) {
            sum += n;
        }
        return reducedProduct(sum);
    }
    public static int reducedProduct(int... numbers) {
        int product = 1;
        for (int n : numbers) {
            product *= n;
        }
        String resultAsString = Integer.toString(product);
        if (resultAsString.length() > 1) {
            String[] numbersOfProd = resultAsString.split("");
            int[] newNumbers = new int[resultAsString.length()];
            for (int i = 0; i < newNumbers.length; i++) {
                newNumbers[i] = Integer.parseInt(numbersOfProd[i]);
            }
            return reducedProduct(newNumbers);
        }
        return product;
    }
}
