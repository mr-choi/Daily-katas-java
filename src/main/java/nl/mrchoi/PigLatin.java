package nl.mrchoi;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
KATA: English to Pig Latin Translator
Pig Latin has two very simple rules:
1. If a word starts with a consonant move the first letter(s) of 
the word, till you reach a vowel, to the end of the word and 
add "ay" to the end.
have ➞ avehay
cram ➞ amcray
take ➞ aketay
cat ➞ atcay
shrimp ➞ impshray
trebuchet ➞ ebuchettray
2. If a word starts with a vowel add "yay" to the end of the 
word.
ate ➞ ateyay
apple ➞ appleyay
oaken ➞ oakenyay
eagle ➞ eagleyay
Write two functions to make an English to pig Latin translator. The
first function translateWord(word) takes a single word and 
returns that word translated into pig latin. The second function 
translateSentence(sentence) takes an English sentence and 
returns that sentence translated into pig Latin.
*/

public class PigLatin {
    public static String translateWord(String word) {
        if (word.length() == 0) return "";
        Pattern vowel = Pattern.compile("[aeiou]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = vowel.matcher(word);
        if (!matcher.find()) return word + "ay";
        int pos = matcher.start();
        if (pos == 0) return word + "yay";
        String firstPart = (Pattern.compile("^[A-Z]").matcher(word).find()) ?
            word.substring(pos, pos+1).toUpperCase() + word.substring(pos+1) :
            word.substring(pos);
        return firstPart + word.substring(0, pos).toLowerCase() + "ay";
    }

    public static String translateSentence(String sentence) {
        Pattern letterWord = Pattern.compile("[a-z]+", Pattern.CASE_INSENSITIVE);
        Matcher matcher = letterWord.matcher(sentence);
        return matcher.replaceAll((match) -> translateWord(match.group(0)));
    }
}
