package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RomanToNumberTest {
    @Test
    public void shouldPassExamples() {
        assertEquals(2006, RomanToNumber.romanToNumber("MMVI"));
        assertEquals(1944, RomanToNumber.romanToNumber("MCMXLIV"));
    }
}
