package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PasswordStrengthTest {
    @Test
    public void shouldSucceedTestcases() {
        assertEquals(PasswordStrength.passwordStrength("password"), "Weak");
        assertEquals(PasswordStrength.passwordStrength("11081992"), "Weak");
        assertEquals(PasswordStrength.passwordStrength("@S3cur1ty"), "Strong");
        assertEquals(PasswordStrength.passwordStrength("!@!pass1"), "Moderate");
        assertEquals(PasswordStrength.passwordStrength("mySecurePass12"), "Moderate");
        assertEquals(PasswordStrength.passwordStrength("pass word"), "Invalid");
        assertEquals(PasswordStrength.passwordStrength("stonk"), "Invalid");
    }
}
