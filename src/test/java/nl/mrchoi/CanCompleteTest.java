package nl.mrchoi;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CanCompleteTest {
    @Test
    public void shouldPassExamples() {
        assertTrue(CanComplete.canComplete("butl", "beautiful"));
        assertFalse(CanComplete.canComplete("butlz", "beautiful"));
        assertFalse(CanComplete.canComplete("tulb", "beautiful"));
        assertFalse(CanComplete.canComplete("bbutl", "beautiful"));
    }

    @Test
    public void shouldPassCustomTestcases() {
        assertTrue(CanComplete.canComplete("tall", "waterfall"));
        assertTrue(CanComplete.canComplete("old", "wonderlands"));
        assertTrue(CanComplete.canComplete("world", "wonderlands"));
        assertFalse(CanComplete.canComplete("world", "wordless"));
    }
}
