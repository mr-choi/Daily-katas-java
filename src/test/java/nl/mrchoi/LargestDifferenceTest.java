package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LargestDifferenceTest 
{
    @Test
    public void succeedsExample()
    {
        int[] numbers = {2,3,1,7,9,5,11,3,5};
        assertEquals(LargestDifference.largestDifference(numbers), 10);
    }

    @Test
    public void considersSmallerMustBeBeforeLarger()
    {
        int[] numbers = {2,3,11,7,9,5,1,3,5};
        assertEquals(LargestDifference.largestDifference(numbers), 9);
    }
}
