package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ReductoMultiplicationTest {
    @Test
    public void shoudPassTestcases() {
        assertEquals(0, ReductoMultiplication.sumDigProd(0));
        assertEquals(9, ReductoMultiplication.sumDigProd(9));
        assertEquals(7, ReductoMultiplication.sumDigProd(9, 8));
        assertEquals(6, ReductoMultiplication.sumDigProd(16, 28));
        assertEquals(1, ReductoMultiplication.sumDigProd(111111111));
        assertEquals(2, ReductoMultiplication.sumDigProd(1, 2, 3, 4, 5, 6));
        assertEquals(6, ReductoMultiplication.sumDigProd(8, 16, 89, 3));
        assertEquals(6, ReductoMultiplication.sumDigProd(26, 497, 62, 841));
        assertEquals(6, ReductoMultiplication.sumDigProd(17737, 98723, 2));
        assertEquals(8, ReductoMultiplication.sumDigProd(123, -99));
        assertEquals(8, ReductoMultiplication.sumDigProd(167, 167, 167, 167, 167, 3));
        assertEquals(2, ReductoMultiplication.sumDigProd(98526, 54, 863, 156489, 45,6156));
    }
}
