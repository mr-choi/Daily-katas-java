package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PigLatinTest {
    @Test
    public void translateWord_shouldPassTestcases() {
        assertEquals("agflay", PigLatin.translateWord("flag"));
        assertEquals("Appleyay", PigLatin.translateWord("Apple"));
        assertEquals("uttonbay", PigLatin.translateWord("button"));
        assertEquals("", PigLatin.translateWord(""));
        assertEquals("odaytay", PigLatin.translateWord("today"));
    }

    @Test
    public void translateSentence_shouldPassTestcases() {
        assertEquals("Iyay ikelay otay eatyay oneyhay afflesway.", PigLatin.translateSentence("I like to eat honey waffles."));
        assertEquals("Oday ouyay inkthay ityay isyay oinggay otay ainray odaytay?", PigLatin.translateSentence("Do you think it is going to rain today?"));
    }
}
