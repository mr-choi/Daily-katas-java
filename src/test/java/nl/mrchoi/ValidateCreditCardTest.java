package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ValidateCreditCardTest {
    @Test
    public void shouldPassTestcases() {
        assertEquals(false, ValidateCreditCard.validate("79927398714"));
        assertEquals(false, ValidateCreditCard.validate("79927398713"));
        assertEquals(true, ValidateCreditCard.validate("709092739800713"));
        assertEquals(false, ValidateCreditCard.validate("1234567890123456"));
        assertEquals(true, ValidateCreditCard.validate("12345678901237"));
        assertEquals(true, ValidateCreditCard.validate("5496683867445267"));
        assertEquals(false, ValidateCreditCard.validate("4508793361140566"));
        assertEquals(true, ValidateCreditCard.validate("376785877526048"));
        assertEquals(false, ValidateCreditCard.validate("36717601781975"));
    }
}
