package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FreddyFridaysTest {
    @Test
    public void succeedsExample() {
        assertEquals(FreddyFridays.freddyFridays(2021), 1);
        assertEquals(FreddyFridays.freddyFridays(2026), 3);
    }
}
