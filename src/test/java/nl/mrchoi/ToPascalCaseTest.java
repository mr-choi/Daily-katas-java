package nl.mrchoi;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ToPascalCaseTest {
    @Test
    public void succeedsBasicTestcases() {
        assertTrue(ToPascalCase.toPascalCase("pineapple on pizza").equals("PineappleOnPizza"));
        assertTrue(ToPascalCase.toPascalCase("The quick brown fox jumped over the lazy dog").equals("TheQuickBrownFoxJumpedOverTheLazyDog"));
        assertTrue(ToPascalCase.toPascalCase("a new world").equals("ANewWorld"));
    }
    @Test
    public void handlesMultipleSpaces() {
        assertTrue(ToPascalCase.toPascalCase("hello  world").equals("HelloWorld"));
    }
    @Test
    public void handlesCapitalLetters() {
        assertTrue(ToPascalCase.toPascalCase("The qUICK brown FOX jUmPs OvEr").equals("TheQuickBrownFoxJumpsOver"));
    }
    @Test
    public void removesNonWordCharacters() {
        assertTrue(ToPascalCase.toPascalCase("The qUick!  bRoWn fox  ; jumped, OVER the    lazy. dog").equals("TheQuickBrownFoxJumpedOverTheLazyDog"));
    }
}
