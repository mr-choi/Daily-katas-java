package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ChallengeTest {
    @Test
    public void shouldPassTestcases() {
        assertEquals("zero", Challenge.numToEng(0));
        assertEquals("twenty six", Challenge.numToEng(26));
        assertEquals("five hundred nineteen", Challenge.numToEng(519));
        assertEquals("one hundred six", Challenge.numToEng(106));
        assertEquals("six hundred", Challenge.numToEng(600));
        assertEquals("nine hundred ninety nine", Challenge.numToEng(999));
    }
}
