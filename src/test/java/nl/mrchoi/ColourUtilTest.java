package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ColourUtilTest {
    @Test
    public void shouldPassExamples() {
        assertEquals(true, ColourUtil.validRgbColour("rgb(0,0,0)"));
        assertEquals(true, ColourUtil.validRgbColour("rgb(255,255,255)"));
        assertEquals(true, ColourUtil.validRgbColour("rgba(0,0,0,0)"));
        assertEquals(true, ColourUtil.validRgbColour("rgba(255,255,255,1)"));
        assertEquals(true, ColourUtil.validRgbColour("rgba(0,0,0,0.123456789)"));
        assertEquals(true, ColourUtil.validRgbColour("rgba(0,0,0,.8)"));
        assertEquals(true, ColourUtil.validRgbColour("rgba( 0 \t ,  127   \n    ,\t55\t, \t     0.1  )"));
        assertEquals(true, ColourUtil.validRgbColour("rgb(0%,50%,100%)"));
        assertEquals(false, ColourUtil.validRgbColour("rgb(0,,0)"));
        assertEquals(false, ColourUtil.validRgbColour("rgb (0,0,0)"));
        assertEquals(false, ColourUtil.validRgbColour("rgb(0,0,0,0)"));
        assertEquals(false, ColourUtil.validRgbColour("rgba(0,0,0)"));
        assertEquals(false, ColourUtil.validRgbColour("rgb(-1,0,0)"));
        assertEquals(false, ColourUtil.validRgbColour("rgb(255,256,255)"));
        assertEquals(false, ColourUtil.validRgbColour("rgb(100%,100%,101%)"));
        assertEquals(false, ColourUtil.validRgbColour("rgba(0,0,0,-1)"));
        assertEquals(false, ColourUtil.validRgbColour("rgba(0,0,0,1.1)"));
    }
}
