package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookstoreDiscountTest {
    @Test
    public void shouldPassTestcases() {
        assertEquals(0, BookstoreDiscount.price(), 1e-7);
        assertEquals(8, BookstoreDiscount.price(0), 1e-7);
        assertEquals(8, BookstoreDiscount.price(1), 1e-7);
        assertEquals(8, BookstoreDiscount.price(2), 1e-7);
        assertEquals(8, BookstoreDiscount.price(3), 1e-7);
        assertEquals(8*3, BookstoreDiscount.price(1,1,1), 1e-7);
        assertEquals(8*2*0.95, BookstoreDiscount.price(0,1), 1e-7);
        assertEquals(8*3*0.9, BookstoreDiscount.price(0,2,3), 1e-7);
        assertEquals(8*4*0.8, BookstoreDiscount.price(0,1,2,3), 1e-7);
        assertEquals(8 + (8*2*0.95), BookstoreDiscount.price(0,0,1), 1e-7);
        assertEquals(2*(8*2*0.95), BookstoreDiscount.price(0,0,1,1), 1e-7);
        assertEquals(8*4*0.8+8*2*0.95, BookstoreDiscount.price(0,0,1,2,2,3), 1e-7);
    }
}
