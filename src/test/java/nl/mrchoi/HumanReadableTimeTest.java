package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HumanReadableTimeTest {
    @Test
    public void shouldPassTestcases() {
        assertEquals("00:00:00", HumanReadableTime.humanReadableTime(0));
        assertEquals("00:00:05", HumanReadableTime.humanReadableTime(5));
        assertEquals("00:01:00", HumanReadableTime.humanReadableTime(60));
        assertEquals("23:59:59", HumanReadableTime.humanReadableTime(86399));
        assertEquals("24:00:00", HumanReadableTime.humanReadableTime(86400));
        assertEquals("99:59:59", HumanReadableTime.humanReadableTime(359999));
    }
}
