package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TelephoneNumberTest {
    @Test
    public void shoudPassExamples() {
        assertEquals("123-647-3937", TelephoneNumber.textToNum("123-647-EYES"));
        assertEquals("(325)444-8378", TelephoneNumber.textToNum("(325)444-TEST"));
        assertEquals("653-879-8447", TelephoneNumber.textToNum("653-TRY-THIS"));
        assertEquals("435-224-7613", TelephoneNumber.textToNum("435-224-7613"));
    }
}
