package nl.mrchoi;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ValidNameTest {
    @Test
    public void shouldPassTestcases() {
        assertTrue(ValidName.validName("Herbert Wells"));
        assertTrue(ValidName.validName("H. Wells"));
        assertTrue(ValidName.validName("H. G. Wells"));
        assertTrue(ValidName.validName("Herbert G. Wells"));
        assertTrue(ValidName.validName("Herbert George Wells"));
        assertFalse(ValidName.validName("Herbert"));
        assertFalse(ValidName.validName("h. Wells"));
        assertFalse(ValidName.validName("H Wells"));
        assertFalse(ValidName.validName("H. George Wells"));
        assertFalse(ValidName.validName("Herbert George W."));
        assertFalse(ValidName.validName("Herb. George Wells"));
    }
}
