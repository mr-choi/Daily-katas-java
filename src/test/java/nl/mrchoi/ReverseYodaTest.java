package nl.mrchoi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ReverseYodaTest {
    @Test
    public void shouldPassExamples() {
        assertEquals("I shall hit you with my stick.", ReverseYoda.reverseYoda("Hit you with my stick, I shall."));
        assertEquals("My crush has rejected me. I need ketamine.", ReverseYoda.reverseYoda("Rejected me, my crush has. Ketamine, I need."));
        assertEquals("I am an alien. The government is holding me captive in Area 51.", ReverseYoda.reverseYoda("An alien, I am. Holding me captive in Area 51, the government is."));
    }
}
